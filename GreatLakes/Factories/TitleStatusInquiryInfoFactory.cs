﻿using GreatLake.Common;
using GreatLakes.Entities;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Text;

namespace GreatLakes.Factories
{
    public static class TitleStatusInquiryInfoFactory
    {
        public static TitleStatusInquiryInfo GetTitleStatusInquiryInfoByVin(string vin)
        {
            try
            {
                var url = "https://apps.ilsos.gov/regstatus/StatusCheck?command=index&nbr=" + vin + "&type=VIN&submit=Submit";
                var web = new HtmlWeb();
                var doc = web.Load(url, "POST");

                var vinform = doc.GetElementbyId("vinform");
                var resultList = vinform.ChildNodes[1].ChildNodes[9].ChildNodes[3].ChildNodes[1];

                TitleStatusInquiryInfo info = new TitleStatusInquiryInfo();

                Dictionary<string, string> htmlMap = new Dictionary<string, string>();
                for (int i = 1; i < 22; i += 2)
                {
                    if (i > resultList.ChildNodes.Count - 1)
                    {
                        break;
                    }

                    if (resultList.ChildNodes[i].ChildNodes[1].InnerText.Trim() == HtmlFieldName.IsLienholder && htmlMap.ContainsKey(HtmlFieldName.IsLienholder))
                    {
                        htmlMap.Add(HtmlFieldName.LienholderAddress, resultList.ChildNodes[i].ChildNodes[3].InnerText.Trim());
                    }
                    else
                    {
                        htmlMap.Add(resultList.ChildNodes[i].ChildNodes[1].InnerText.Trim(), resultList.ChildNodes[i].ChildNodes[3].InnerText.Trim());
                    }
                }

                info.VIN = htmlMap[HtmlFieldName.VIN] ?? "";

                if (htmlMap.ContainsKey(HtmlFieldName.TitleNumber))
                {
                    info.TitleNumber = htmlMap[HtmlFieldName.TitleNumber] ?? "";
                }
                else
                {
                    info.TitleNumber = "";
                }
                
                if (htmlMap.ContainsKey(HtmlFieldName.ApplicationNumber))
                {
                    info.ApplicationNumber = htmlMap[HtmlFieldName.ApplicationNumber] ?? "";
                }
                else
                {
                    info.ApplicationNumber = "";
                }
                
                if (htmlMap.ContainsKey(HtmlFieldName.MostRecentTitleIssuance))
                {
                    info.MostRecentTitleIssuance = htmlMap[HtmlFieldName.MostRecentTitleIssuance] ?? "";
                }
                else
                {
                    info.MostRecentTitleIssuance = "";
                }

                if (htmlMap.ContainsKey(HtmlFieldName.TypeOfTitle))
                {
                    info.TypeOfTitle = htmlMap[HtmlFieldName.TypeOfTitle] ?? "";
                }
                else
                {
                    info.TypeOfTitle = "";
                }
                
                if (htmlMap.ContainsKey(HtmlFieldName.IsLienholder))
                {
                    info.IsLienholder = htmlMap[HtmlFieldName.IsLienholder] ?? "";
                }
                else
                {
                    info.IsLienholder = "";
                }

                if (htmlMap.ContainsKey(HtmlFieldName.MultiOwner))
                {
                    info.MultiOwner = htmlMap[HtmlFieldName.MultiOwner] ?? "";
                }
                else
                {
                    info.MultiOwner = "";
                }

                if (htmlMap.ContainsKey(HtmlFieldName.IsBetterAddressNeededForMailing))
                {
                    info.IsBetterAddressNeededForMailing = htmlMap[HtmlFieldName.IsBetterAddressNeededForMailing] ?? "";
                }
                else
                {
                    info.IsBetterAddressNeededForMailing = "";
                }
                
                if (htmlMap.ContainsKey(HtmlFieldName.IsVehicleTaggedAsRebuilt))
                {
                    info.IsVehicleTaggedAsRebuilt = htmlMap[HtmlFieldName.IsVehicleTaggedAsRebuilt] ?? "";
                }
                else
                {
                    info.IsVehicleTaggedAsRebuilt = "";
                }

                if (htmlMap.ContainsKey(HtmlFieldName.IsVehicleTaggedAsHavingFloodDamage))
                {
                    info.IsVehicleTaggedAsHavingFloodDamage = htmlMap[HtmlFieldName.IsVehicleTaggedAsHavingFloodDamage] ?? "";
                }
                else
                {
                    info.IsVehicleTaggedAsHavingFloodDamage = "";
                }
                
                if (htmlMap.ContainsKey(HtmlFieldName.LienholderAddress))
                {
                    info.Lienholder = htmlMap[HtmlFieldName.LienholderAddress]?.Replace("\n", ", ").Replace("\r", "").Replace("\t", "") ?? "";
                }
                else
                {
                    info.Lienholder = "";
                }

                return info; 
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
