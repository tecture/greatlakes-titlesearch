﻿using GreatLakes.StaticVariables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GreatLakes.Factories
{
    public static class CsvFactory
    {
        public static string GenerateTitleStatusInquiryResultCsv(List<IEnumerable<string>> data)
        {
            var columns = new string[]
            {
                ColumnName.LoanNumber,
                ColumnName.VIN, 
                ColumnName.TitleNumber, 
                ColumnName.AppNumber, 
                ColumnName.MostRecentTitleIssuance, 
                ColumnName.TypeOfTitle, 
                ColumnName.IsLienholder, 
                ColumnName.MultiOwner, 
                ColumnName.IsBetterAddressNeededForMailing, 
                ColumnName.IsVehicleTaggedAsRebuilt,
                ColumnName.IsVehicleTaggedAsHavingFloodDamage, 
                ColumnName.Lienholder
            };

            return Common.Helper.CreateCsv(columns, data);
        }
    }
}
