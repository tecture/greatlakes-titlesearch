﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GreatLakes.Entities
{
    public class TitleStatusInquiryInfo
    {
        public string VIN { get; set; }
        public string TitleNumber { get; set; }
        public string ApplicationNumber { get; set; }
        public string MostRecentTitleIssuance { get; set; }
        public string TypeOfTitle { get; set; }
        public string IsLienholder { get; set; }
        public string MultiOwner { get; set; }
        public string IsBetterAddressNeededForMailing { get; set; }
        public string IsVehicleTaggedAsRebuilt { get; set; }
        public string IsVehicleTaggedAsHavingFloodDamage { get; set; }
        public string Lienholder { get; set; }

        public void printToConsole()
        {
            Console.WriteLine($"VIN:                                      {VIN}");
            Console.WriteLine($"Title Number:                             {TitleNumber}");
            Console.WriteLine($"Application Number:                       {ApplicationNumber}");
            Console.WriteLine($"Most Recent Title Issuance:               {MostRecentTitleIssuance}");
            Console.WriteLine($"Type of Title:                            {TypeOfTitle}");
            Console.WriteLine($"Lienholder:                               {IsLienholder}");
            Console.WriteLine($"Multi-Owner:                              {MultiOwner}");
            Console.WriteLine($"Is Better Address Needed For Mailing:     {IsBetterAddressNeededForMailing}");
            Console.WriteLine($"Is Vehicle Tagged As Rebuilt:             {IsVehicleTaggedAsRebuilt}");
            Console.WriteLine($"Is Vehicle Tagged As Having Flood Damage: {IsVehicleTaggedAsHavingFloodDamage}");
            Console.WriteLine($"Lienholder:                               {Lienholder}");
        }
    }
}
