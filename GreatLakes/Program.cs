﻿using GreatLakes.Entities;
using GreatLakes.Factories;
using GreatLakes.UI;
using HtmlAgilityPack;
using System;

namespace GreatLakes
{
    public class Program
    {
        public static async System.Threading.Tasks.Task Main(string[] args)
        {
            await CmdUserInterface.CmdUIAsync();
        }
    }
}
