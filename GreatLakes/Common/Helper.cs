﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GreatLakes.Common
{
    public static class Helper
    {
        public static string CreateCsv(IEnumerable<string> columnNames, IEnumerable<IEnumerable<string>> rows, string separator = ",")
        {
            var csv = new StringBuilder();
            var columnCount = columnNames.Count();

            csv.AppendLine(string.Join(separator, columnNames));

            foreach (var row in rows)
            {
                if (row.Count() != columnCount)
                {
                    throw new FormatException("The length of all rows must match the length of columnNames.");
                }

                // Put data in quotes to allow for commas in CSV format
                var newLine = string.Join(separator,
                    separator == "," ? row.Select(x => $"\"{x?.Trim() ?? ""}\"")
                                     : row);

                csv.AppendLine(newLine);
            }

            return csv.ToString();
        }
    }
}
