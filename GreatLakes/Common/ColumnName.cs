﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GreatLakes.StaticVariables
{
    public static class ColumnName
    {
        public static string LoanNumber = "Loan_number";
        public static string VIN = "VIN";
        public static string TitleNumber = "Title Number";
        public static string AppNumber = "App Number";
        public static string MostRecentTitleIssuance = "Most Recent Title Issuance";
        public static string TypeOfTitle = "Type of Title";
        public static string IsLienholder = "Lienholder";
        public static string MultiOwner = "Multi-Owner";
        public static string IsBetterAddressNeededForMailing = "Is better address needed for mailing";
        public static string IsVehicleTaggedAsRebuilt = "Is vehicle tagged as rebuilt";
        public static string IsVehicleTaggedAsHavingFloodDamage = "Is vehicle tagged as having flood damage";
        public static string Lienholder = "Lienholder";
    }
}
