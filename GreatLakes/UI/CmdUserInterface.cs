﻿using CsvHelper;
using GreatLakes.Entities;
using GreatLakes.Factories;
using GreatLakes.StaticVariables;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;

namespace GreatLakes.UI
{
    public static class CmdUserInterface
    {
        public static async System.Threading.Tasks.Task CmdUIAsync()
        {
            Console.WriteLine("Please input the file directory or drag the file into this console: ");
            string inputFileDir = Console.ReadLine().Trim('"');
            while (!File.Exists(inputFileDir))
            {
                Console.WriteLine("File not exist!!");
                Console.WriteLine("Please input the file directory or drag the file into this console: ");
                inputFileDir = Console.ReadLine();
            }

            Console.WriteLine("Please input an existing directory for result file output: ");
            string outputFileDir = Console.ReadLine().Trim('"');
            while (!Directory.Exists(outputFileDir))
            {
                Console.WriteLine("Directory not exist!!");
                Console.WriteLine("Please input an existing directory for result file output: ");
                outputFileDir = Console.ReadLine();
            }

            Console.WriteLine("Parsing the input file...");

            DataTable dataTable = new DataTable();
            List<string> headerList = new List<string>();

            // Read file to data table
            try
            {
                if (File.Exists(inputFileDir))
                {
                    using (StreamReader streamReader = new StreamReader(inputFileDir))
                    {
                        using (var csv = new CsvReader(streamReader, CultureInfo.InvariantCulture))
                        {
                            using (var dr = new CsvDataReader(csv))
                            {
                                dataTable.Load(dr);
                            }
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Input file not exist!!");
                    return;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to parse the file!!");
                Console.WriteLine($"Details:\n{e.Message}");
                return;
            }

            // Make sure first column is "Loan_number", second column is "VIN"
            var headers = dataTable.Columns;
            foreach (var header in headers)
            {
                headerList.Add(header.ToString());
            }
            if (headerList[0] != ColumnName.LoanNumber)
            {
                throw new Exception($"Wrong file format - First column is not \"{ColumnName.LoanNumber}\"");
            }
            if (headerList[1] != ColumnName.VIN)
            {
                throw new Exception($"Wrong file format - Second column is not \"{ColumnName.VIN}\"");
            }

            Console.WriteLine("Sending requests to https://apps.ilsos.gov/regstatus/StatusCheck website...");

            List<IEnumerable<string>> data = new List<IEnumerable<string>>();
            List<string> failedVINs = new List<string>();
            int count = 0;
            int succeedCount = 0;
            int failedCount = 0;
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                count++;

                string loanNumber = dataTable.Rows[i][ColumnName.LoanNumber].ToString();
                string vin = dataTable.Rows[i][ColumnName.VIN].ToString();

                Console.Write($"Sending request and parsing HTML for VIN: {vin}");
                TitleStatusInquiryInfo info = TitleStatusInquiryInfoFactory.GetTitleStatusInquiryInfoByVin(vin);
                if (info == null)
                {
                    Console.Write(" - ");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("Failed\n");
                    Console.ForegroundColor = ConsoleColor.White;
                    failedVINs.Add(vin);
                    failedCount++;
                    continue;
                }
                
                data.Add(new string[]
                {
                    loanNumber, 
                    info.VIN, 
                    info.TitleNumber, 
                    info.ApplicationNumber, 
                    info.MostRecentTitleIssuance, 
                    info.TypeOfTitle, 
                    info.IsLienholder, 
                    info.MultiOwner, 
                    info.IsBetterAddressNeededForMailing, 
                    info.IsVehicleTaggedAsRebuilt, 
                    info.IsVehicleTaggedAsHavingFloodDamage, 
                    info.Lienholder
                });

                Console.Write(" - ");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("Succeed\n");
                Console.ForegroundColor = ConsoleColor.White;
                succeedCount++;
            }

            Console.WriteLine();
            Console.WriteLine("----------------------------------------------------------------");
            Console.WriteLine($"Total:   {count}");
            Console.WriteLine($"Succeed: {succeedCount}");
            
            Console.Write($"Failed:  ");
            if (failedCount > 0) Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(failedCount + "\n");
            Console.ForegroundColor = ConsoleColor.White;

            if (failedCount > 0)
            {
                Console.WriteLine();
                Console.WriteLine("Failed VIN(s): ");
                foreach (string failedVIN in failedVINs)
                {
                    Console.WriteLine(failedVIN);
                }
            } 
            Console.WriteLine();

            string resultStr = CsvFactory.GenerateTitleStatusInquiryResultCsv(data);

            try
            {
                string inputFileName = Path.GetFileName(inputFileDir);
                string outputFileName = $"{inputFileName}_{DateTime.Now.ToString("yyyyMMddhhmmss")}.csv";
                await File.WriteAllTextAsync($"{outputFileDir}\\{inputFileName}_{DateTime.Now.ToString("yyyyMMddhhmmss")}.csv", resultStr);

                Console.WriteLine($"The result file \"{outputFileName}\" has been output to the directory.");
            }
            catch (Exception e)
            {
                Console.Write("Failed to write the file to the directory.");
            }

            Console.WriteLine();
            Console.WriteLine("Press enter to exit...");
            Console.ReadLine();
        }
    }
}
